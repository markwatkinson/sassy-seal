# trap ctrl-c and call ctrl_c()
trap ctrl_c INT



function ctrl_c() {
    kill $SERVER_PID
    exit 0
}
node build.js --include-drafts
echo 'Starting HTTP server...'
pushd public
npx http-server &
SERVER_PID=$!
popd
echo 'Server started'

if ! command -v inotifywait &> /dev/null
then
    echo "inotifywait not found. Please install inotify-tools to use the file watcher. Alternatively, run node build.js to regenerate the site manually. "
    exit 1
fi

while true; do

# exclude hidden files by searching for '/.' in the
# path
inotifywait -e modify,create,delete -r views content pages static --exclude '/\..+'  && \
    node build.js --include-drafts

done
