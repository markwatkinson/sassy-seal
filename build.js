const fs = require('node:fs');
const { execSync } = require('child_process');

const config = require("./site.json");

const opts = {
    watch: false,
    includeDrafts: false
}



///////////////////////////
// Setting up external deps
///////////////////////////

const Marked = require('marked').Marked;
const markedHighlight = require('marked-highlight').markedHighlight;
const prism = require('prismjs');
const loadLanguages = require('prismjs/components/');
const fm = require('front-matter');
const { error } = require('node:console');
let marked;

function setupPrismJs() {
    console.log('Setting up prism');
    loadLanguages();
    marked = new Marked(
        markedHighlight({
            langPrefix: 'prism language-',
            highlight(code, lang, info) {
                let r = code;
                if (prism.languages[lang]) {
                    r = prism.highlight(code, prism.languages[lang], lang);
                }
                return '<pre class="language-' + lang + '">' + r + "</pre>";
            }
        })
    );
    console.log('Set up prism');
}


// This is written to by a hook inside Marked, so it
// is hard to encapsulate away from a global variable.
// Note it will get overwritten every time a post
// is parsed. 
let frontMatter;
function resetFrontMatter() {
    frontMatter = {};
}
function getFrontMatter() {
    return frontMatter;
}

function setupFrontmatter() {
    const parsers = {
        date: x => new Date(x),
        lastUpdated: x => new Date(x),
        _: x => x
    };

    // Override function
    function preprocess(markdown) {
        resetFrontMatter();
        const { attributes, body } = fm(markdown);
        for (const prop in attributes) {
            const parser = parsers[prop] || parsers._;
            let parsed = parser(attributes[prop]);
            getFrontMatter()[prop] = parsed;
        }
        return body;
    }

    marked.use({ hooks: { preprocess } });
}

function setupHtmlOverride() {
    marked.use({
        extensions: [
            {
                name: 'codespan',
                renderer(token) {
                    return `<code class="language-">${token.text}</code>`;
                },
            },
            {
                name: 'link',
                renderer(token) {
                    const isExternal = token.href.startsWith('http');
                    const clazz = isExternal ? 'external' : '';
                    const target = isExternal ? '_blank' : '_self';
                    return `<a href="${token.href}" class="${clazz}" target="${target}">${token.text}</a>`;
                }
            },
            {
                name: 'heading',
                renderer(token) {
                    const link = token.text
                        .replace(/[^a-zA-Z0-9_\- ]/g, '')
                        .replace(/[\s]+/g, '-')
                        .toLowerCase();

                    return `<h${token.depth} id=${link}>${token.text}</h${token.depth}>`;
                }
            }]
    })
}

function setupMarked() {
    setupPrismJs();
    setupFrontmatter();
    setupHtmlOverride();
}


function validatePreamble(preamble, required) {
    if (!preamble) throw new Error('No preamble defined');
    for (const prop of required) {
        if (!preamble[prop]) { throw new Error('Preamble definition ' + prop + ' missing')}
    }
}

///////////////////////////
// Post parsing
///////////////////////////

const POST_TYPE = {
    POST: {
        code: 'POST',
        preambleRequired: ['title', 'date', 'slug'],
        slugPrefix: '/posts/'
    },
    WIKI: {
        code: 'WIKI',
        preambleRequired: ['title', 'slug', 'lastUpdated'],
        slugPrefix: '/knowledge/'
    },
}

function parsePost(path, type) {
    const post = {};
    const fileData = fs.readFileSync(path, {'encoding': 'utf8'});
    const text = fileData.trim().replace(/\r\n|\r|\n/g, '\n');
    post.rendered = marked.parse(text);
    post.preamble = getFrontMatter();


    validatePreamble(post.preamble, type.preambleRequired);
    post.url = type.slugPrefix + post.preamble.slug;
    return post;
}

function listFiles(dir, type) {
    console.log('Reading files in', dir)
    const files = fs.readdirSync(dir, { recursive: true });
    console.log('Read files')
    let posts = files.filter(x => x.endsWith('.md'))
        .map(fn => {
            const path = dir + '/' + fn;
            try {
                return parsePost(path, type);
            }
            catch (ex) {
                console.error("Couldn't parse post " + path + ", due to error: " + ex);
                console.error(ex);
                return null;
            }
        }).filter(x => x);

    if (!opts.includeDrafts) {
        posts = posts.filter(x => !x.preamble.draft);
    }
    return posts;
}

function listPosts() {    
    let posts = listFiles('content/posts', POST_TYPE.POST);
    
    posts = posts.filter(x => x.preamble['date'])
    posts.sort((a, b) => a.preamble['date'] - b.preamble['date']);
    posts.reverse();


    for (let i = 0; i < posts.length; i++) {
        let next = posts[i - 1];
        let prev = posts[i + 1];
        posts[i].prev = prev;
        posts[i].next = next;
    }
    return posts;
}

function listWiki() {
    let posts = listFiles('content/wiki', POST_TYPE.WIKI);
    posts.sort((a, b) => (a.title || '').localeCompare(b.title || ''));
    return posts;

}
///////////////////////////
// Template rendering
///////////////////////////

function renderTemplate(template, vars) {
    vars = vars || {};
    vars._site = config;
    function _render(code) {

        // These functions are accessible within template files
        function render(view, vars) {
            let template = fs.readFileSync(view, {'encoding': 'utf8'});
            return renderTemplate(template, vars);
        }

        const linkTo = (path) => path;
        const t = (obj) => obj instanceof Date ? obj.toDateString() : obj;
        
        let result = '';
        try {
            result = eval(code);
        } catch(ex) {
            console.error('Error rendering template ', ex);
        }
        return result;
    }

    const rendered = template.replace(/<\?js\s([\s\S]*?)\?>/g, ($0, $1) => _render($1))
    return rendered;
}

function renderRoot(templatePath, rootPath, vars) {
    let template = fs.readFileSync(rootPath, {'encoding': 'utf8'});
    vars = vars || {};
    vars.page = templatePath;
    return renderTemplate(template, vars);
}

function buildRoute(page, vars) {
    vars.route = vars.route.replace(/\/_index$/, '');
    const rootPath = './views/_root.html';
    const targetDir = './public' + vars.route;
    const target = targetDir + '/index.html';
    const rendered = renderRoot(page, rootPath, vars);

    fs.mkdirSync(targetDir, { recursive: true });
    fs.writeFileSync(target, rendered);
}

function buildRss(items) {
    const target = './public/rss.xml';

    const itemsXml = items.map(x => `
        <item>
            <title>${x.title}</title>
            <description>${x.description}</description>
            <link>${x.link}</link>
            <pubDate>${x.pubDate.toUTCString()}</pubDate>
            <guid>${x.guid}</guid>
        </item>
    `).join('\n');
    const xml = `<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>${config.author.name}</title>
        <description>${config.description}</description>
        <link>${config.url}</link>
        <atom:link href="${config.url}/rss.xml" rel="self" type="application/rss+xml" />
        <copyright>${new Date().getFullYear()} ${config.author.name} All rights reserved</copyright>
        <lastBuildDate>${new Date().toUTCString()}</lastBuildDate>
        <pubDate>${new Date().toUTCString()}</pubDate>
        ${itemsXml}
    </channel>
</rss>
     `
     fs.writeFileSync(target, xml);

}

function buildAll() {

    setupMarked();
    const posts = listPosts();

    console.log('Retrieved posts:')
    const colors = ['\x1b[34m', '\x1b[35m'];
    for (let i = 0; i < posts.length; i++) {
        let p = posts[i];
        console.log(colors[i % colors.length] + '  -  ' + p.preamble.date.toISOString().substring(0, 10) + ' | ' + p.preamble.title)
        console.log('                  ' + p.preamble.description + '\x1b[0m');
    }

    const wiki = listWiki();
    console.log('Retrieved wiki:');
    for (let i = 0; i < wiki.length; i++) {
        let p = wiki[i];
        console.log(colors[i % colors.length] + '  -  ' + p.preamble.title +'\x1b[0m');
    }



    buildRoute('pages/index.html', {route: '/'});
    buildRoute('pages/posts.html', {route: '/posts/', posts: posts});
    buildRoute('pages/cv.html', {route: '/cv/'});
    buildRoute('pages/404.html', {route: '/404'});

    const rssItems = [];

    posts.forEach(p => {
        const route = '/posts/' + p.preamble.slug;
        const link = config.url + route;

        buildRoute('views/post.html', {route: route, post: p, title: `${p.preamble.title} | ${config.author.name}`});
        rssItems.push({
            title: p.preamble.title,            
            description: p.preamble.description,
            link,
            pubDate: p.preamble.date,
            guid: link
        })
    });
    wiki.forEach(p => {
        let route = '/knowledge/' + p.preamble.slug;
        const link = config.url + route;
        buildRoute('views/wiki.html', {route: route, post: p, title: `${p.preamble.title} | ${config.author.name}`});
        rssItems.push({
            title: p.preamble.title,            
            description: p.preamble.description,
            link,
            pubDate: p.preamble.lastUpdated,
            guid: link
        })
    })

    fs.cpSync('static', './public/', {recursive: true});
    execSync('npx sass ./static/style/main.scss:./public/style/main.css');

    buildRss(rssItems);

}

function clean() {
    const publicPath = './public';
    if (fs.existsSync(publicPath))
        fs.rmSync(publicPath, {recursive: true});
    fs.mkdirSync(publicPath)
}


function rebuildAll() {
    const start = +new Date();
    console.log('Regenerating all pages');
    console.log('Cleaning...');
    clean();
    console.log('Cleaned');
    console.log('Building all');
    buildAll();
    console.log('Built all')
    const end = +new Date();
    console.log(`Finished in ${(end - start) / 1000}s`)
}


function main() {
    opts.includeDrafts = process.argv.indexOf('--include-drafts') >= 0;
    rebuildAll();
}

main();
