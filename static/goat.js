(function () { 
    var location = window.location;   

    function logUrl() {
        var url = location.pathname + location.search + location.hash;

        function write() {
            window.goatcounter.count({
                path: url,
            })
        }

        function schedule() {
            if (window.goatcounter && window.goatcounter.count) {
                write();
            }
            else {
                setTimeout(function() {
                    schedule();
                }, 1000);
            }
        }
        schedule();
    }

    var url = location.href;
    logUrl();

    setInterval(function() {
        var changed = url !== location.href;
        url = location.href;
        if (changed) {
            logUrl();
        }
    }, 1000);
}());
