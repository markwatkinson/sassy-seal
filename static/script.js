(function() {
    'use strict';

    function initTheme() {

        const localStorageDarkModeKey = 'darkMode';
        const darkThemeFromLocalStorage = localStorage.getItem(localStorageDarkModeKey);

        let isDark = false;

        if (darkThemeFromLocalStorage !== null) {
            console.log('Enforcing theme %s from local storage', darkThemeFromLocalStorage ? 'dark' : 'light');
            isDark = darkThemeFromLocalStorage === 'true';
        }


        const setTheme = (themeName) => {

            console.log('Setting theme', themeName);

            const html = document.querySelector('html');
            [...html.classList]
                .filter(x => x.startsWith('theme-'))
                .forEach(x => html.classList.remove(x));
            html.classList.add(themeName);
        }

        const setTooltipFromDarkMode = () => {
            const darkModeToggleBtn = document.querySelector('#nightModeToggle');
            if (darkModeToggleBtn) {
                darkModeToggleBtn.setAttribute('title',`Switch to ${isDark ? 'light' : 'dark'} mode`);
            }
        }

        const setThemeFromDarkMode = () => {
            const theme = isDark ? 'theme-dark': 'theme-light';
            setTheme(theme);            
        }

        document.addEventListener("DOMContentLoaded", function(event) { 
            const darkModeToggleBtn = document.querySelector('#nightModeToggle');
            if (darkModeToggleBtn) {
                darkModeToggleBtn.addEventListener('click', event => {
                    isDark = !isDark;
                    setThemeFromDarkMode();
                    setTooltipFromDarkMode();
                    console.log('Saving theme %s to local storage', isDark ? 'dark' : 'light');
                    localStorage.setItem(localStorageDarkModeKey, isDark);
                });
                setTooltipFromDarkMode();
            }
        });
        

        if (darkThemeFromLocalStorage === null && window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            isDark = true;
        }

        setThemeFromDarkMode();
    }

    function initHamburger() {
        let isHamburgerShown = false;

        const setHamburgerShown = (state) => {
            const sideBar = document.querySelector('.sidebar');
            const body = document.querySelector('body');
            if (!state) {
                sideBar.classList.remove('show');
                body.classList.remove('sidebar-overlay');
            }
            else {
                sideBar.classList.add('show');
                body.classList.add('sidebar-overlay');
            }
            isHamburgerShown = state;
        }

        document.addEventListener("DOMContentLoaded", (event) => { 
            const toggleBtn = document.querySelector('#hamburger');
            toggleBtn.addEventListener('click', event => {
                setHamburgerShown(!isHamburgerShown);
            });
        });

        document.addEventListener('click', (event) => {
            if (!isHamburgerShown) {
                return;
            }
            const sideBar = document.querySelector('.sidebar');
            const buttons = document.querySelector('.top-buttons');

            const isSidebarclick = buttons.contains(event.target)
                || sideBar.contains(event.target);

            if (!isSidebarclick) setHamburgerShown(false);
            
        })
    }

    document.addEventListener('scroll', (event) => {
        const pos = window.scrollY;
        if (pos > 20) {
            document.body.classList.add('scroll-20');
        }
        else {
            document.body.classList.remove('scroll-20');
        }
    })

    document.addEventListener("DOMContentLoaded", function() {
        document.querySelectorAll('.data-table td').forEach(cell => {
            if (!isNaN(cell.textContent.trim()) && cell.textContent.trim() !== "") {
                cell.classList.add('numeric');
            }
        });
    });

    initTheme();
    initHamburger();

    


})();