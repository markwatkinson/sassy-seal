#!/usr/bin/env bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

read -p "Post file name: " fn

date=$(date '+%Y-%m-%d')
datestring=$(date '+%Y-%m-%d')T20:00:00.000Z

git checkout -b "post-$fn"

retVal=$?
if [ $retVal -ne 0 ]; then
    exit $retVal
fi

path="content/posts/$date---$fn.md"

cat << EOF > $SCRIPTPATH/$path
---
title: ""
date: "$datestring"
template: "post"
draft: true
slug: ""
description: ""
---


EOF

retVal=$?
if [ $retVal -ne 0 ]; then
    exit $retVal
fi

echo "Created $path"
