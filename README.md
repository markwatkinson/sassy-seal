# Sassy Seal Static Site 

A simple static site generator.

## Usage:

```
npm install
./run.sh
```

There will now be a HTTP server listening on http://localhost:8080 serving the site.

Changes to any of the relevant files should automatically regenerate the site.

The output of the site generation is under public/

## Licence

Code is provided under AGPL3.

Content (i.e. any file under content/) is provided under [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/)
