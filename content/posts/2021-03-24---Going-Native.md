---
title: "Going native?"
date: "2021-03-24T20:00:00.000Z"
template: "post"
draft: false
slug: "going-native-app-development"
category: "Development"
tags:
  - "Apps"
  - "Native"
  - "Web"
description: "Android recently pushed a faulty update to the WebView component, which broke a surprising number of popular apps that you might expect to be native"
---

Google recently [pushed a faulty update to Android's WebView component](https://asgaard.co.uk/news/google-issues-fix-for-android-app-crashes/), which broke a lot of apps.

This probably piqued the interest of a lot of app developers, because it gives an insight into how many apps aren't really fully native.

The native vs cross-platform debate is a long standing discussion, having moved through a lot of different stages. When you're creating an app, do you write it natively, using Java/Kotlin and Objective-C/Swift, or do you use a cross platform framework? Phonegap, Cordova, and, later, Ionic, bet on native being too much work and the web being better supported. These frameworks essentially serve a local website running inside a browser, or a WebView component, and present this wrapped up neatly as an app. Later, other cross-platform ideas emerged, but the WebView approach remains a well trodden path.

There's always a perception in app development that native is the 'premium' version you pursue if you have the budget to do so. The idea is that you can get decent and possibly even good results with cross platform approaches, but you'll always be second best to a native app. Whether or not that's really fair is a matter of discussion, but it's certainly a prevalent view.

What's interesting about the recent issue with the WebView is how many big apps stopped working. Gmail, Amazon, Facebook, Audible, Deezer... all of these are big companies with a lot of money behind them, and apparently they all use WebViews. This doesn't mean the entire app is non-native, but it shows that native is a bit of a misnomer.

A few years ago, I worked on a native iOS app (Objective-C), designed for a very specific use case involving audio processing. A big part of the app is a visualisation of the audio levels or frequencies it's picking up, which the user uses to calibrate a physical system in the real world. This is how it looks:

![Ampetronic Loopworks](/media/loopworks.png)


Despite being a native app, the visualisation in the middle is inside a webview. It's a custom made graph using the JavaScript library D3.js. Why? Because D3.js makes it pretty easy to create these kinds of visuals, and, at the time at least, we didn't find anything native that would let us get the same results as easily. It worked really well and looks and responds seamlessly within the rest of the app. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/yn7jYkPZgSY?start=210" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Although it often gets derided, web technology is actually mature, well supported, and there exist many well made libraries that can let you do some things much more easily than through any other method.

As well as things like D3.js, it's also important to remember that creating layouts and documents in HTML is fairly easy and universal. 

I don't know what Gmail actually uses its webview for, but if I was creating a mobile email client, I'd probably render email content inside a webview. Emails are (usually) HTML so it just makes sense to pass that down to a webview instead of trying to translate it to something native. In the case of Amazon and Facebook, a lot of the content they're rendering is also available on web, which means there exist HTML templates to set out all the information of the products or posts. Again, although I might write all the navigation and app chrome natively, if I was creating an Amazonesque or Facebookesque app, I'd probably render most of the remote content inside a webview for interoperability and to cut down on a lot of native specific UI code.

The question of native vs non-native isn't necessarily as straightforward as it looks. Just because you choose native doesn't mean you should exclude non-native UI.