---
title: "Bose QuietComfort (2023 model)"
date: "2024-09-12T20:00:00.000Z"
template: "post"
draft: false
slug: "bose-quietcomfort-2023"
description: "Fun with noise cancelling headphones"
---

I'm a new owner of a pair of Bose QuietComfort (2023 model) noise cancelling headphones.

This is my first experience with active noise cancellation, and I have some opinions.

My choice came down to one between the Bose QuietComfort, Sony XM4 and Sennheiser Momentum 4. I listened to some audio samples on YouTube and found the XM4s sound quality to be very poor, while the Momentum 4's sound quality is very good but its ANC is supposedly significantly worse than the other two's. The QuietComfort seemed to sit in the middle ground between the other two in terms of sound quality and ANC.

I opted for the QuietComfort SC edition, which is exactly the same as the non-SC edition, except they come with a soft case instead of a hard case. These are currently retailing for £195 versus £259, so it's up to you to decide if a hard case is worth £64.

<div class='contents'>

- [General Overview and Miscellaneous Points](#general-overview-and-miscellaneous-points)
- [Sound Quality](#sound-quality)
  - [EQ Settings](#eq-settings)
- [Noise Cancellation](#noise-cancellation)

</div>

## General Overview and Miscellaneous Points

Most importantly for me, they work fine with Linux ✅

They support AAC and SBC/SBC-XQ. No low latency modes. The latency is noticeable in real time situations. That's Bluetooth for you, unfortunately.

The controls on the headphones are physical buttons. They offer on the right cup: an on/off slider, volume up, volume down and a pause/play button. On the left cup is a button to cycle through ANC presets, which also doubles as an 'Action' button on long press. I know that the QuietComfort Ultra model has some kind of touch control, and... I'm glad this one doesn't, to be honest. The physical buttons work well.

- ANC presets are by default are just 'Quiet' (100%) and 'Aware' (0%). You can set up your own presets but a preset is really just a percentage slider with a toggle for wind blocking mode. The aware mode acts as a pass-through, which amplifies external noise over the passive noise cancellation.  
- The action button is configurable to either state the battery level or to do something with Spotify. I can't comment on this as Spotify is a mess and I don't use it.

Battery life is advertised at around 24 hours. I haven't timed it but it's somewhere in that region.

As well as Bluetooth, there is a 2.5mm analog connection and they come with a 2.5mm to 3.5mmm wire. Apparently they need to be switched on when in analog, but I haven't tried it and probably won't.

The headphones' voice prompt is crudely robotic, but that's probably because it synthesises speech based on device names. e.g. it will tell you the headphones are connected to "pixel 7" or "mark fedora" (my laptop's hostname).


## Sound Quality

I'm not an audiophile but I'm also not deaf. I play the guitar and I've spent enough time fiddling around with EQs and mixes to know what sound is supposed to sound like. 

I listen to mostly rock and metal, and I think that the stock experience of the QuietComfort is poor, because there's too much bass and it drowns out everything else. Metal often employs a 'scooped' mix in studio recordings, where the bass and treble are pumped high and the mids cut, because it tends to make things sound powerful. Bands like Metallica heavily scoop their guitars whereas Pantera heavily scooped the whole mix. In general they are already doing this as much as they can without it being detrimental to the overall sound, so if the headphones then add even more bass then it becomes ridiculous. 

To me, the QuietComforts have too much bass and not enough mids. From the samples I've heard, it's not anywhere near the level of the comically bad Sony XM4s, but it's still poor. A lot of consumer audio hardware over emphasises the bass because it's the first thing anyone notices and bass feels powerful, so this isn't unusual, but it's still frustrating at this price point that headphone manufacturers won't make music sound like the artist intended it to.

Fortunately, they are very responsive to EQing, and by cutting the bass a little and pushing the mids up, I've got them sounding relatively flat (to my ears anyway). The Bose app has a built in 3 band EQ, which seems to be stored on the headphones, but I don't find 3 bands nearly enough. I EQ them with Easy Effects on my PC and PowerAmp on my phone. As a side note, I have an article on [applying a device specific EQ on Linux](/knowledge/linux/device-specific-audio-eq), if you are you interested.

But I should say although they sound good now, they certainly aren't amazing. I also have a pair of Audio-Technica ATH-M30X headphones, which are just bog standard wired, non noise cancelling headphones and cost about a third of the price of the QuietComfort. The sound quality of the ATH-M30X is clearly superior. The sound is crisper and better defined, with seemingly more space between the instruments. I might get a little closer with more EQ tweaking, but it's obvious that QuietComfort headphones aren't made to compete with wired, non noise cancelling headphones, despite being three times the price.

So I'd say that at least at this price point you have to choose whether you want Bluetooth and ANC or whether you want sound quality. I'm not an audiophile and I can hear their shortcomings, but they're good enough to not significantly impact my enjoyment. If you are an audiophile, don't get these headphones.

### EQ Settings

My current PC EQ settings are as follows. To my ears, this sounds like a reasonably flat response that brings out the sounds I care about. I will almost certainly continue to tweak this, though.


<a href='/media/audio-device-eq/eq.png' target=_blank>![Plasma](/media/audio-device-eq/eq.png)</a>

```
Filter 1: ON PK Fc 25.848932 Hz Gain -2 dB Q 2.209714
Filter 2: ON PK Fc 40.967796 Hz Gain -2 dB Q 2.209714
Filter 3: ON PK Fc 64.92958 Hz Gain 0 dB Q 2.209714
Filter 4: ON PK Fc 102.90645 Hz Gain 1.5 dB Q 2.209714
Filter 5: ON PK Fc 163.09573 Hz Gain 2 dB Q 2.209714
Filter 6: ON PK Fc 258.48932 Hz Gain 3 dB Q 2.209714
Filter 7: ON PK Fc 409.67795 Hz Gain 5 dB Q 2.209714
Filter 8: ON PK Fc 649.29584 Hz Gain 8 dB Q 2.209714
Filter 9: ON PK Fc 1029.0646 Hz Gain 7 dB Q 2.209714
Filter 10: ON PK Fc 1630.9574 Hz Gain 4 dB Q 2.209714
Filter 11: ON PK Fc 2584.8933 Hz Gain 2 dB Q 2.209714
Filter 12: ON PK Fc 4096.78 Hz Gain 3 dB Q 2.209714
Filter 13: ON PK Fc 6492.958 Hz Gain 3 dB Q 2.209714
Filter 14: ON PK Fc 10290.6455 Hz Gain 0 dB Q 2.209714
Filter 15: ON PK Fc 16309.573 Hz Gain 0 dB Q 2.209714
```


I use a few songs to evaluate sound quality and adjust EQ. I'll list them below with the reasons I like them, plus, they're all songs I've heard many, many times. If what I say means something to you then you might care about my opinions more than if it doesn't. If you mostly listen to rap or EDM then you can probably discard everything I say.

**Devin Townsend - Kingdom** (Epicloud version), because it's a barrage of sound with a lot of texture

**Metallica - Enter Sandman**. The guitar tone on this whole album was state of the art when it was released; it is extremely carefully produced to give a thick and bass heavy sound. When the rhythm starts properly at 00:56 there's also a lot of high end from the cymbals, so there's plenty to work with. This is an amazing mix to evaluate sound quality, because 1. it's powerful, 2. it's clear, and 3. it's already so bass heavy that if the speakers are playing silly games then suddenly it loses all its detail.

**Iron Maiden - Two Minutes To Midnight**. The original 1984 version was produced by Martin Birch who created some crystal clear recordings with Iron Maiden. He mixed some of the classic Deep Purple and Rainbow albums too which also sound great, but I think he really peaked with Iron Maiden at the end of his career, benefiting from many years of experience up until that point. You could really take any of Iron Maiden's 80s releases, but Powerslave and Somewhere In Time are probably my favourites for the production. With Two Minutes, there's a lot of texture in just the single guitar in the intro, and then perfect clarity when the rest of the band come in. Iron Maiden recordings are a little unusual for metal in that the mix tends to be a bit sparse with the bass having a bright sound with its own very distinct place in the mix, which you lose if there's too much bass in the speakers. Sadly later remasters (1998 and 2015) are easier to find but are slightly muddier.

**AC/DC - Highway to Hell**. AC/DC are an outlier in this list in that they use relatively little distortion (and instead just hit the strings really hard) and have quite a sparse sound, so there's a lot of tonal detail in that intro riff.

**Death - Crystal Mountain**. Very clean production and a powerful and percussive mix.

**Nile - Annihilation of the Wicked**. The intro section is a wall of sound with a lot of ambient atmospheric noises. I think the guitar tones are a bit sludgy, but I do like the sonic effect of the single lead guitar cutting through the ambience. 


## Noise Cancellation

My use case for noise cancellation is that I work from home and have noisy family members and a dog, who can be pretty vocal when someone has the audacity to enter his territory (i.e. anywhere he can see through the window).

Firstly, the passive noise cancellation is actually pretty good even without switching the headphones on. There's a good seal between the cups and my head, which cuts down on a lot of ambience right from the start.

The ANC works pretty well for cutting down on distracting noises. I can generally still hear general things I probably want to (e.g. the doorbell), but it sounds a lot more distant. I can just about hear people talking to me as long as they're speaking relatively loudly, but, again, it sounds more distant, so nearby conversations aren't as distracting. As for my dog, I can still hear him bark but it's not nearly as intense. The noise cancellation takes a lot of punch out of his bark and it sounds more like he's in a different room rather than a couple of metres away.

Furthermore, if I have even some quiet music playing then the external sound is further suppressed and less likely to distract me.

Overall I'm sold on ANC.

I did find initially that the ANC made my ears feel weird. The sudden absence of low frequencies seems to be a bit of a surprise to your ear, which perceives their absence as a form of pressure and gives the feeling of your ear needing to pop. This was quite noticeable to begin with, but not so much anymore. When listening to music the effect is reduced.

