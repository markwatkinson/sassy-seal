---
title: "Steam Deck save game syncing for non-Steam games"
date: "2023-04-26T20:00:00.000Z"
template: "post"
draft: false
slug: "steam-deck-sync"
description: "Quick and dirty save game syncing on the Steam Deck, for games not covered by Steam cloud"
---

I have a Steam Deck and I really like it. I've been playing a lot of Civ VI on it, which I happen to own on the Epic Games store. Due to it being a non-Steam game, the Deck doesn't offer any functionality to sync my save games between my PC and my Steam Deck. 

The Steam Deck is really just a Linux box, so it's not really that difficult to put something together that handles syncing, it just requires a little knowledge of all the moving parts.

There are a few ways of doing this. I opted to use Dropbox as the file store, because I wanted sync to work without both the PC and Deck being online. An obvious alternative would be to use Syncthing, but this is peer to peer and would need both machines to be online at the same time. If you have a local NAS, you could use that instead and mount it through fstab. But I went for Dropbox, because it works for me.

Experience told me that putting this together on Linux would be easy but Windows would need a bit more fiddling. Actually, in this case, it was the opposite. Windows works easily, Linux a bit less so.

## Windows

On Windows I set up a directory junction redirecting my saves folder to Dropbox and that works fine.

To do this, open up `%UserProfile%\Documents\My Games\Sid Meier's Civilization VI (Epic)`, copy the `Saves` folder to somewhere else (e.g. `Saves-old`), run this command from an elevated command prompt:

```
mklink /J "%UserProfile%\Documents\My Games\Sid Meier's Civilization VI (Epic)\Saves" "%UserProfile%\Dropbox\games\CivVISaves\Saves"
```

Windows Explorer will show this with the shortcut icon:

![](/media/deck/explorer.png)

and then copy the contents of `Saves-old` back into `Saves`. The save games are now stored directly in the Dropbox folder, and Dropbox will sync them. Job done!


## Steam Deck

The save games on the Deck are stored under `~/Games/Heroic/Prefixes/SidMeiersCivilizationVI/pfx/drive_c/users/steamuser/My\ Documents/My\ Games/Sid\ Meier\'s\ Civilization\ VI\ \(Epic\)/Saves`

I tried replacing this directory with a symlink to `~/Dropbox/games/CivVISaves/Saves`, but for whatever reason, the game could not see any existing saves nor would it create new ones. I don't know if Wine has some issues following symlinks; I couldn't really find anything definite. There is a [bug report](https://github.com/ValveSoftware/steam-runtime/issues/334) which suggests that Proton intentionally limits filesystem access to within the game's container directory, but I didn't have any success with the `STEAM_COMPAT_MOUNTS` env var so it might be a different issue.

In the end I just wrote a two line script on the Deck to copy files between Dropbox and the game's save directory. I then added the script as a non-Steam game, so I can easily invoke it manually from gaming mode when I need to trigger a sync. You could add this within a filesystem watcher and put it in a service to make it start automatically, but I didn't really need it.



Putting it all together, the basic process is:
1. Make the Deck easier to access by a) Enabling SSH, or b) Plugging in a keyboard and mouse
2. Install Dropbox and get it starting automatically in gaming mode
3. Create the sync script
4. Add the sync script as a non-Steam game.

### 1. Enable SSH on the Deck

Seth Hendrick has written a comprehensive article on [enabling SSH](https://shendrick.net/Gaming/2022/05/30/sshonsteamdeck.html).

You probably don't really need to if your Deck is only ever connected to a private local network, but I opted to set up key based authentication as in my opinion you can never truly trust all devices on your network.

### 2. Install Dropbox

The easiest way to install Dropbox on the Deck is to boot it into desktop mode and open up the Discover application. After it's installed, find Dropbox in the Applications menu and Dropbox will guide you through sign-in.

Dropbox seems to start automatically in Desktop mode but not in gaming mode. To get it to start up when the Deck boots, so you need to create a systemd script.

Create the script:  `~/.config/systemd/user/dropbox.service` with contents:
```
[Unit]
Description=Dropbox

[Service]
Type=simple
ExecStart=flatpak run com.dropbox.Client
Restart=on-failure
RestartSec=20
RestartForceExitStatus=0

[Install]
WantedBy=default.target
```

then run these commands:

```
$ systemctl --user enable dropbox
$ systemctl --user start dropbox
```

`enable` will configure the service to start at boot, and `start` will make it start right now.

### 3. Create the sync script

Create `~/bin/sync.sh` with contents:

```
#!/bin/sh

cp -Rnp /home/deck/Games/Heroic/Prefixes/SidMeiersCivilizationVI/pfx/drive_c/users/steamuser/My\ Documents/My\ Games/Sid\ Meier\'s\ Civilization\ VI\ \(Epic\)/Saves ~/Dropbox/games/CivVISaves/

cp -Rnp  ~/Dropbox/games/CivVISaves/Saves/ /home/deck/Games/Heroic/Prefixes/SidMeiersCivilizationVI/pfx/drive_c/users/steamuser/My\ Documents/My\ Games/Sid\ Meier\'s\ Civilization\ VI\ \(Epic\)
```

and then

```
$ chmod +x ~/bin/sync.sh
```

These commands will copy recursively (-R) the Deck's saves to Dropbox, and Dropbox's saves to the Deck. The -n means 'no clobber', i.e. it won't overwrite existing files, and -p means preserve attributes (like timestamp).

You can then ssh and run this manually whenever you want to sync, or add it as a non-Steam game so you can run it at the press of a button.

### 4. Add the sync script to Steam

Boot the Deck into desktop mode, open up Steam and select `Games ->  Add a Non-Steam Game to My Library`

![](/media/deck/non-steam.png)

Then choose 'browse' and locate and select ~/bin/sync.sh.

That's it.

Now in your Steam library, even in gaming mode, you should have a game called 'sync.sh':

![](/media/deck/sync-game.jpg)

And it should have properties:

![](/media/deck/sync-properties.jpg)


So, when you want to sync, all you need to do is run it from Steam. It should take a second, then exit, and then Dropbox will actually upload your save games. Obviously there will be a slight delay for Dropbox to upload any new files, so don't switch off your Deck immediately after running it.

Overall I find the Steam Deck to be pretty fantastic for providing a strong out of the box experience but also allowing you to easily tinker with things like this if you want to.