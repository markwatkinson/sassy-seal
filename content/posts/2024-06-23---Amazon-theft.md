---
title: "Amazon"
date: "2024-06-23T20:00:00.000Z"
template: "post"
draft: false
slug: "amazon-returns"
description: "Amazon has not proved a reliable retailer for expensive items"
---

A few months ago I ordered an SSD and RAM for my laptop, with a total cost of around £275, From Amazon. The next day, I received a package containing a packet of Wellwoman vitamin tablets, with approximate value £8. Amazon handled it well initially and sent the correct items the following day, with the stipulation that if I didn't return the vitamins within 30 days they'd charge me for the replacement items, i.e. another £275.

That's all well and good, except I did return them several months ago and there's still nothing to indicate that they've processed the return. The only status I have is "You've already requested a return of this item. Return requested" with a replacement order ID. This was over two months ago.

A few years ago I had a mouse that stopped working after 6 months or so. Amazon sent me a replacement free of charge. Or at least, that's what I thought. I can't remember the details, but I do remember they did actually charge me for it, to my surprise, many months later. So I'm not satisfied that this is resolved yet.

In my view, Amazon has a growing reputation problem in that their pages are full of Chinese items with badly photoshopped promotional images. At best this makes the site look cheap and unreliable, and at worst, dangerous. For example, would you trust a random phone charger on Amazon not to burn your house down? If you buy from a high street shop you expect it to conform to British electrical safety standards. From Amazon, well, I'd be less convinced. The items I bought here were of reputable brands and directly from Amazon rather than marketplace sellers so should be immune to this criticism, and yet Amazon failed to provide a smooth experience on that side of the market too. 

If they'd resolved this promptly I'd have thought no more of it, but they didn't, and as a result I've pretty much stopped buying from Amazon and won't be renewing my Prime subscription when it expires in July.

If Amazon's cheaper items are a minefield of junk and you can't trust them with expensive items, what's left?

-----

I think they also have a mismatch here with Alexa. Alexa seems to have been intended to make it easier to *buy things* from Amazon. But Alexa doesn't make it easier to *shop* on Amazon; it makes it harder. Amazon search results are chaotic and require energy to work through. The average search result is so full of junk and seemingly duplicate items that it's hard to imagine saying "Alexa, buy X" and ending up with what you wanted.

----

Update 25/7/24: My Prime has now expired and they have still not processed the return!