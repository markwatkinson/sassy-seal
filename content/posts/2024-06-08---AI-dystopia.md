---
title: "AI Dystopia"
date: "2024-06-07T20:00:00.000Z"
template: "post"
draft: false
slug: "ai-dystopia"
description: "Google and Microsoft's use of AI is not currently in users' interests"
---

AI is a big thing in the news since ChatGPT was released a couple of years ago. Every company seems to be rushing to add AI into their products. It's important to state that by 'AI', I mean generative AI, like LLMs.

Personally, despite the hype I'm yet to see any improvements as a result of AI to any software I use, and I see it as being a similar hype bubble that cryptocurrency was a few years earlier. 

But a few things really make me scratch my head.

## Google Search

Firstly, Google has been integrating AI summarisation into Google Search. This, infamously, has recently recommended adding glue to pizza to ensure the cheese remains stuck, because the suggestion was in its training data and LLMs aren't intelligent. Regurgitating nonsense from training data is one of two classes of problem that LLMs have. The second is inventing nonsense that isn't represented verbatim in the training data. This second problem has commonly been referred to as 'hallucinating'. However, LLMs are probabilistic models and this 'hallucation' is just the process of how they work. They sequentially stream words from their training data in an order that is linguistically probable. They are machines of language and probability and that they can deal in facts or logic is not necessarily a safe expectation.

I think it's obvious that Google has lost the plot with regards to Search over the last 5 years or so, in as much that Search quality now is very poor. Partly that's because there's an ever increasing amount of nonsense on the internet with an increasing number of people trying to game search results. Partly it's because of their own poor decisions. One poor decision is rolling back a policy where they would previously penalise websites that showed different results to the crawler as to the end user. Another poor decision is that they focus on very commercialised websites instead of independent blogs. The end result is that Search is not as useful as it was 10 years ago and it frequently gives back very spammy results or results that don't contain what you searched for.

Search used to have a slight advantage in that they it was simply a way of finding other content. Directing people to low quality content will (and has) hurt Google's reputation in the long term, but in the short term they're a bit more insulated from reputational damage than they would be if they were directly publishing low quality content. Using an LLM to put low quality content directly on their page is a questionable strategy at best because it is now Google itself that is publishing nonsense.

## Microsoft Recall

Recall is Microsoft's planned Windows feature to video the user's desktop and archive it to be searchable by AI. This is just bonkers from a privacy and security perspective. There are all kinds of things that appear on people's screens that shouldn't be saved anywhere. A good example is that a password manager that will encrypt stored passwords but display them in plaintext. Windows Recall effectively undermines the security a password manager provides. But there are plenty of other problems. It may mean your PC stores other people's personal data which would previously have been transient, which raises a whole raft of legal questions. The behaviour is extremely overreaching. 

If this had landed 20 years ago it would have been called 'spyware'.

I can see the thought process of "wow, what if we could give the user a way of looking back at what they were doing two weeks ago", but this is very much a "just because you can..." situation where there is a small potential benefit and huge potential risk. Or perhaps I'm being generous and it's the first stage of a long term plan to analyse user behaviour using their own hardware and electricity for the purpose of targeted advertising within the operating system, in which case the previous risk/reward is irrelevant.

Personally, I remotely access a Windows PC owned by my employer and that's the only interaction I have with Windows now. I have two personal laptops and a desktop, and they all run various flavours of Linux. My decision to ditch Windows as much as possible predates Microsoft's Recall announcement, but it's gone from "I might be convinced to run Windows if I really needed to" to a complete non-starter.