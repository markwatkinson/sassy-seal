---
title: "Stratford's Big Half Marathon"
date: "2024-11-01T20:00:00.000Z"
template: "post"
draft: false
slug: "stratford-big-half-marathon"
description: "Fun in the sun at the Stratford-upon-Avon Big Half Marathon, and a significant PB"
---

I ran the Stratford's Big Half Marathon on Sunday. This is the September event organised by Tempo Events and is not the same as the Shakespeare Half Marathon held in April, though the routes overlap a bit. I targeted it for a good time, and had a lot of fun doing so.

## Background

My fastest half marathon was Birmingham 2019, where I ran a 4:23/k pace for the whole thing. Unfortunately, the whole thing was only about 17-18k because a 'suspicious vehicle' forced a last minute course change. This would have equated to around a 1:32 finish time assuming I'd kept the pace for the last few kilometres but since then I've really under-performed and my actual PB was around 1:44. Partly this is due to ineffective training, but mostly it's been bad luck with weather or course conditions. We have had a lot of heatwaves over the past few years!

I decided to target the Big Stratford Half this year as it's a fairly flat route and was held at the end of September so I hoped the weather would be favourable.

I'd pencilled in around or just under a 4:45/k pace to try to sneak it just under 1:40. I felt this was a bit conservative but it was also very achievable, unlikely to leave me blowing up, and I'd be happy with the PB.

As it happened, the date was postponed from late September to late October due to heavy rain flooding parts of the course by the river, which I was fairly pleased with as it gave me another four weeks of training and I hoped October would bring cooler weather (Spoiler alert: it didn't).


## Training

Training had gone well.

I generally keep my running base well maintained and have been in a good position for the last few months. I ran Solihull in August and managed a very narrow PB of 1:43 without much speed work, though I faded on the hills in the last 5k. After that I brought in more of a focus on speed/threshold work, which would generally look like 4x10 minutes or 2x20 minutes with 2-3 minute jog recoveries in the middle of longer runs. Saturday Parkruns were either at full 5k effort or threshold, depending how I felt. I'd topped out my long run at 25k or 2:15 in preparation for the original date. I was averaging about 70k a week across five days, with two of those sessions being some kind of workout.

Unusually, I was very ready for my taper week by the time it came around which probably indicates my training load was at the very edge of what I could handle. 

We were only informed on the Friday afternoon before the Sunday that the race was postponed, so making the most of the taper, I ran a long run of 27k on the Sunday. I think the taper helped a lot and I was able to resume the same overall intensity for the next few weeks, feeling much stronger.

To mix things up over the structured threshold work I switched to a weekly 21.1k progressive run. I wanted to do another 25-27k long run a week and a half before but had to abort it at 22k with some inner ankle pain (which I've had before and means my calves are tight). This was really the only run that didn't go to plan. I neglected short interval speed work and could have benefited from it just for the neuromuscular benefits to running economy and turnover, but you can't focus on everything and I didn't want to expose myself to the injury risk late in training.

## The Course

The course looks like a standard road/pavement affair if you're not familiar with it, but it's not. The first 5k section along the Stratford Greenway (part of which is run twice as a little loop), then it's road for a while before another 4k on the greenway and then a final kilometre on road/footpaths into Stratford town centre with the last 100m or so on grass.

The greenway needs a little explanation. It's an old railway line that's been converted into a footpath. It's overall flat as a pancake, though there are small bumps as you intersect with footpaths. Which makes it sound easy, but it's not.

The first part of the greenway you encounter after 5k as you diverge from the 10k route. 10k runners enter the greenway heading right, towards the town centre, whereas the HM route enters going left, away from the centre. This segment is basically a dirt trail and lasts for approximately 4k. It was reasonably dry and firm with small sections of mud that were generally easy to avoid, though it's not quite as easy and consistent as road running as you do have to weave around rough sections and mud. 

After completing this section the route does a big loop around via the roads and takes you back to the point you diverged from the 10k route, where, at around 15-16k, you enter the greenway once again, heading towards the town centre. This second section is horrible. It's some kind of concreted surface with gravel on the top. It's extremely fatiguing to run on. The concrete is rock hard and the gravel moves underneath you. This is the point you really need to focus on keeping your form in check, else it'll just collapse. You will slow down here.

Other than this, the course has a couple of hills but nothing massively challenging. The first few kilometres are a bit bumpy with an uphill start, and there's a very steep downhill at around 3.5k. Garmin recorded 68m of elevation with most of that being in the first 5k. It's reasonably flat as far as half marathons go.

Overall I wouldn't call it a fast course, because of the greenway. But it's not a hilly course either.

## How it went

With the extra four weeks of training I was suspecting that my 4:45 pace was probably a bit conservative, but fitness was perhaps counterbalanced by my having had quite bad allergies for the past few weeks as well as having glorious sunshine on the day. The sun is still pretty warm even in late October and I could feel it heating my face standing around at the start. So I decided to stick to the plan and not go out too fast. I was a little concerned by the sun based on past experience, but I was dressed for heat and had 600mls of liquid with me (with Tailwind) so I was prepared.

The first 5k were pretty congested with 10k runners but as soon as we turned onto the greenway it quietened off. A group of three of us ran the whole stretch of the greenway together in single file, with me in the easy position at the back behind a small lady in yellow and in front of her a very tall man in red (and it's not often I look at someone and think "wow, you're tall"). I was quite grateful they were keeping a solid pace at around my target pace so I just stuck to them. The yellow top lady stopped at the water station at the end of the greenway and fell back behind me but she was with us for quite a while after that. There's a 1 mile loop around the end of the greenway and by the time we did the second lap of this we were weaving around runners on their first lap, particularly at the water station.

I stuck with the man in red along the roads until shortly before we rejoined the greenway when I started to push a little bit harder and he seemed to fall back a bit. I didn't see him again. I was feeling strong until we re-entered the greenway, whereupon the transition from smooth tarmac was immediately obvious. This stretch was tough. I was alone for most of it and cursing the decision to plot a HM course over a loose gravel path. My previously strong-feeling legs were tiring quickly and my watch was telling me my ground contact time had shot up. I kept the pace up but it was costing me a lot of energy just to maintain what had previously been very comfortable. Half way along the seemingly never ending greenway I overtook a lady in a blue Wye Valley Runners shirt who casually asked "it's less than six k now, right?". It was actually more like 4, but it felt like 6. It was a relief when I finally exited the greenway into Stratford centre, but I'd burnt so much energy on the greenway that my attempt to pick up the pace for the last kilometre wasn't really happening and topped out at about 4:35/k as the Wye Valley lady sailed past me with about 500m to go, having obviously paced herself better!

Never mind, it still came out as 1:38, a 5 minute PB. 

## Thoughts

Stratford is a relatively small race with maybe a couple of thousand runners of a range of abilities. It's not like the big city based events where there are spectators and support all the way around, but you wouldn't expect that in a more rural location. Overall it was well organised and there was plenty of water on the course.

Obviously I am incredibly pleased with the PB but I actually do not think it's a fast course. The greenway comes at probably the worst time in a HM -- at around 15k you're either starting to speed up a bit and looking to burn your remaining energy, or you're realising you've gone out a little too hot and you're desperately trying to not slow down. The greenway ensures that whichever aim you have at this point, you will not achieve it. I suspect, from looking at my splits and given how strong I was feeling immediately before entering the greenway, that it probably cost me 90-120 seconds in the last few kilometres, both on the greenway and in the fatigue preventing an effective kick through the easier conditions of the town centre.

My ground contact time shows an interesting story.

<a href='/media/20241101/stratford-gct.png' target=_blank>![Ground contact time](/media/20241101/stratford-gct.png)</a>

And my heart rate was also steady until the same point, then started coming up:

<a href='/media/20241101/stratford-hr.png' target=_blank>![Heart rate](/media/20241101/stratford-hr.png)</a>

And my pace? Well, that did the opposite.

<a href='/media/20241101/stratford-pace.png' target=_blank>![Heart rate](/media/20241101/stratford-pace.png)</a>


It's pretty obvious where the gravelled greenway starts from how long I was spending on the ground. It really is a big drag on your efficiency.

In summary though, I enjoyed it and there's a good chance I'll be doing it again next year. But the greenway will definitely affect my pacing strategy. I am also looking for a full marathon to do in the spring, but I'm in two minds as to whether the Shakespeare Stratford marathon will be it as its route goes twice down the greenway!