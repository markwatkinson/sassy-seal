---
title: "Great Birmingham Run 2024"
date: "2024-05-10T20:00:00.000Z"
template: "post"
draft: false
slug: "great-birmingham-run"
description: "Or: Why I'll be running Stratford Half Marathon next year instead"
---

I've run the Great Birmingham Run every year it's been on since 2018 and this year was no exception, but I probably won't do it again next year.

Pre-COVID, the 10k was held in May and the half marathon in May. Post COVID, both are held on the same day in May. In 2023 and again in 2024, the May date coincided with a very sudden warm turn of the weather. 2023 was particularly bad as it was pretty much unexpected even a few hours before, whereas in 2024 at least we had a few days of it first. But the effect is largely the same - you train in very different conditions than the event. 

I learnt from last year and took my own bottle with Tailwind (electrolytes + fuel). I had another sachet on me that I could mix up later if necessary, but it wasn't. I skipped the first water station (5k) and then took another bottle at 11k, and another at 16k. I drank some of it and poured some of it over my head (as you do). That's a lot of water, and I was still a bit dehydrated!

But while I learnt from last year, I don't feel the organisers did. Three water stations on a half is the minimum I'd expect of a responsible organiser, and in unusual heat I'd expect more. In comparison, on a hot day in 2023, the Two Castles event set out three water stations over a 10k route - that's twice as many per kilometre! Plus, this is just water we're talking about. Where are the electrolytes? Great Run isn't some amateur organiser on a shoe-string budget, it's a national chain who charge over £40 for entry. I think they could afford to give out some isotonic water at one of the stations. Overall I'm not impressed.

Furthermore, there's the course itself. The 2023/2024 course is hard, and, in my opinion, harder than previous years. The first 5k twist in circles around the Jewellery Quarter, which is very up and down and often gives you only a few hundred meters between hard 90 and even 180 degree turns. People always think of uphills as hard but steep downhills are challenging too - either you take them really fast with all the impact that involves (good luck, quads), or you slow yourself by fighting against gravity, which is very inefficient. I'm surprised how achy my legs feel afterwards. 

In the last few kilometres the course merges back with the 10k route, which with the wave timing means the fastest wave of the half marathon ends up merging with the slowest wave of the 10k. So you suddenly find hundreds of people in front of you who are running far slower than you, which is exactly as chaotic as it sounds.

Great Run sent me an email straight afterwards offering me a discount on their projected price next year of £45. Stratford a few weeks earlier in April 2025 looks like a much smarter choice.