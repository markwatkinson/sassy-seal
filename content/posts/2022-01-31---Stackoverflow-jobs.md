---
title: "RIP Stack Overflow Jobs"
date: "2022-02-01T20:00:00.000Z"
template: "post"
draft: false
slug: "stack-overflow-jobs"
description: "The surprising demise of one of the better jobs boards"
---

Stack Overflow [has announced that it will be killing off its jobs board from April.](https://meta.stackoverflow.com/questions/415293/sunsetting-jobs-developer-story) 

I can only say that I find this extremely surprising, and a bit questionable.

The state of software recruitment is a bit underwhelming to say the least. Mostly it consists of about a million recruiters emailing developers with messages such as "Hi Mark, I'm sourcing for a role I think you'd be perfect for, when can we have a chat?".

On the rare occasions a developer's inbox doesn't look like a job board, s/he can go and visit an actual job board and see endless streams of adverts written by people with a similar amount of talent for sales as those demonstrated in the email above. Most are laundry lists of requirements that show no attempt to convince the reader to apply. Which is tolerable if you're unemployed and running out of money, but I don't think that describes the larger market very well.

Stack Overflow Jobs is a lot better. The adverts there tend to be well written and reasonably transparent about the things that matter, as well as offering specific information about the development culture there (see [The Joel Test](https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/), named after Joel Spolsky, one of the original founders of Stack Overflow). Overall if you were actively looking for a job, Stack Overflow Jobs was one of very few high quality places to look.

Therefore I'm extremely surprised that Stack Overflow Jobs is being shut down, because there isn't really much (or any?) competition at the level they operated. This will leave a significant gap in the recruitment market. However, being part of Stack Overflow gave Jobs a cultural advantage I'm not sure anyone else will be able to replicate so easily. 