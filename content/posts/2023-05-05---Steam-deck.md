---
title: "What the Steam Deck does right"
date: "2023-05-05T20:00:00.000Z"
template: "post"
draft: false
slug: "steam-deck"
description: "And why I don't think handheld Windows devices like the ASUS ROG Ally are a threat to the Deck"
---

There is a little bit of commotion in certain online communities at the moment regarding the ASUS ROG Ally, which is a handheld gaming console akin to the Valve Steam Deck. People think ASUS is about to eat Valve's lunch by launching a console that's more powerful than the Steam Deck at a comparable price.

I think they're wrong, because I think the Steam Deck has captured something a bit more subtle than simply putting a PC into handheld format, which is what the ROG Ally is. I think that something like the ROG Ally will appeal to the relatively hardcore PC gamer demographic who want raw processing power above all else. Whereas the Steam Deck provides a much more polished user experience. And it does this by doing a few difficult things very well.

## Linux

The first major point about the Steam Deck is that it runs on SteamOS, which is a Linux distribution. When I first started using Linux (many years ago), the idea that a games device would run Linux was absurd. Few if any games had native Linux ports and Wine (a Windows compatibility layer) was impressive when it worked, but its  coverage was nowhere near ready for prime time.

Valve has poured a lot of software engineering resources into getting Wine (under the name Proton) into a state where it can run a large amount of Windows games well. There's nothing theoretically difficult about getting Wine to such a point - it just requires reimplementing the Win32 API. But the practical side of actually doing it well and hiding all the configuration complexity from the user is a huge feat which is greatly beneficial for the Linux ecosystem, for which both the Wine team and Valve deserve major credit.

### Why invest in Linux/Wine/Proton when they could just bundle the Deck with Windows?

Valve put a lot of effort into Linux, whereas ASUS with the Rog Ally has just relied on Windows. ASUS's approach sounds sensible - after all, virtually every gaming PC runs Windows, so why didn't Valve just stick with Windows?

Valve has been trying to reduce reliance on Windows for around the past decade. 

The main reason is that if you run your device on Windows, you're beholden to Microsoft for everything from your target audience's basic user experience to your own experience in trying to develop and distribute software for the platform. 

In terms of general user experience, back in the Windows 7 era it looked like Microsoft cared about keeping its OS competitive with MacOS and Linux, but in the Windows 8/10/11 era it's harder to feel that way. 

In terms of general developer experience, Windows is popular as a distribution target because it comes preinstalled on a large majority of desktop computing devices, not because it's intrinsically better than Linux (or MacOS). Linux is ideologically free and open and this has the practical effect that Valve can customise the underlying OS and resulting user experience to their own preferences, and no single entity like Microsoft has the power to come along and pull the rug from under them. There's a lot going for Linux, from a developer's practical perspective.

I think that the ROG Ally will struggle, because 1. Windows 11 gets in the way of the user a lot, and 2. Windows is a pretty bad touchscreen OS at the best of times, and on a small screen it's going to be even worse. If ASUS wants a hope of providing a good user experience, they're going to have to abstract away every feature of Windows that the general user might need and cover it all up with a touch friendly interface - similar to the Steam Deck's gaming mode. If the user has to go poking around in Windows at all, the magic is going to look pretty thin.

Another point is the device sleep functionality. It's a seemingly minor feature on a gaming PC, but on a hand-held it's incredibly useful to be able to press the power button and have the device sleep mid-game and resume seamlessly a day later with almost the same battery level. It means I can put it down then pick up and play immediately the next time, without having to wait for the OS to load and the game to load.

Sleep works well on SteamOS. On Windows it's a different story. I've used Windows of many variants on many machines personally and for work, and on not a single one of them have I been able to put the machine to sleep and then come back to it a day later feeling confident it will both still be asleep and resume properly when I try to wake it up.

The lack of a reliable sleep feature on a hand-held is a serious drawback.

## Touchpads

The ROG Ally has a console style control scheme of twin joysticks as the primary inputs. 

The Deck has twin joysticks too, but it also has touchpads. The touchpads are basically the same as a laptop's mouse input touchpad, except you control them with your thumbs.

Most PC games are designed for mouse and keyboard input, obviously. Some support gamepads, some don't. The Steam Deck works surprisingly well with games designed for a mouse. The touchpads are not a perfect mouse input, but they're probably the next best thing. 

On my Deck, I've mainly played Fallout New Vegas and Civilization VI. Fallout is (not exactly, but basically) a first person shooter and Civ VI is a mouse driven strategy game. Both of these are mouse heavy, and both work very well with the touchpads.

When I first got my Deck I wasn't really sure how to approach it and tried the joysticks instead of the touchpads. The touchpads are many, many times better than the joystick for emulating a mouse. I don't think I would consider either Civ VI or FNV to be playable without the touchpads. Yes, console gamers manage, but I'm not a console gamer - I, like the target audience for the Steam Deck and the ROG Ally, am a (lapsed) PC gamer to whom a keyboard and mouse feels natural. And these aren't console games - they're PC games designed for a keyboard and mouse.

Without the touchpads, the Steam Deck would be limited to playing games with first class support for gamepads, which isn't really that many of them.

## Summary

The ROG Ally might well have better hardware than the Steam Deck - I don't know. But hardware is only part of the battle. Valve is able to provide a very polished user experience, and this is possible because they've taken control of the software stack all the way down, and they've tried to make the Deck's inputs as PC like as possible. I haven't scratched the surface of just how customisable and powerful the Deck's inputs are, but suffice it to say, I can't see any handheld device coming close to the Deck without a touchpad.