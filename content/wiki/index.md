---
title: "Knowledge"
lastUpdated: "2025-01-05"
draft: false
slug: "_index"
description: ""
---

<div class='no-header-count'>

Here you'll find various pieces of knowledge I've acquired that I find useful to document, mostly Linux related at the moment.

## Linux

-   [AMD colours changing and washed out with power profiles](linux/amd-colours-power-profiles)
-   [Device specific audio EQ](linux/device-specific-audio-eq)
-   [Full disk encryption](linux/full-disk-encryption)
-   [Hardware video decoding in Chrome (on an AMD iGPU on Fedora, at least)](linux/chrome-hardware-decoding)
-   [Legion 5 Slim 5 AMD+NVIDIA compatibility](linux/lenovo-legion-slim-5-amd)
-   [Powering down your idle Nvidia dGPU in Linux](linux/nvidia-dgpu-power)
-   [Storing dotfiles in git](linux/dotfiles-in-git)
-   [SSH agent config](linux/ssh-agent-config)
-   [VSCodium - Enabling the extension marketplace](linux/vscodium-extensions-marketplace)

</div>