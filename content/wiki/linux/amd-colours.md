---
title: "Washed out/changing colours on AMD when changing power profile"
lastUpdated: "2024-12-15"
draft: false
slug: "linux/amd-colours-power-profiles"
description: ""
---

Laptops with an AMD iGPU have a default feature called Vari-Bright, which supposedly saves power by altering the backlight brightness, but also has the effect that colours look washed out. Furthermore, when you switch Power Profiles, the colours will change immediately and then gradually fade (which is probably a bug, but I don't know?).

You can disable this. The upside of disabling it is your colours are a lot nicer and consistent, the downside is possibly higher power draw. It's difficult to measure the power draw impact as you can't toggle this on and off without a reboot, but from my own readings I suspect it's insignificant.

To disable it you need to add `amdgpu.abmlevel=0` to your kernel parameters.

To do so, edit `/etc/default/grub`, and add `amdgpu.abmlevel=0` to the `GRUB_CMDLINE_LINUX` variable, so it reads something like this (yours will be different):

`GRUB_CMDLINE_LINUX="resume=UUID=6009830b-33b0-4d3c-bad3-94983ad62513 rd.lvm.lv=fedora/00 rd.luks.uuid=luks-24c1ea76-7174-4605-98b8-1c95bbfbbe24 rd.lvm.lv=fedora/01 rhgb quiet nmi_watchdog=0 amdgpu.abmlevel=0"`

Then make the update take effect by running

`sudo grub2-mkconfig -o /etc/grub2.cfg`

And reboot. 