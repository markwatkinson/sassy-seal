---
title: "Enabling hardware video decoding in Chrome on Fedora + AMD"
lastUpdated: "2024-08-08"
draft: false
slug: "linux/chrome-hardware-decoding"
description: ""
---


Hardware video decoding in Chrome is very temperamental and also complicated. Chrome, Chromium and even Brave can have different behaviour on the same machine. Overall I'd recommend Firefox instead, but certain websites like GeForce Now force the use of Chrome.


I have found that running Chrome with this command will enable hardware decoding on my laptop with an AMD iGPU on Fedora 40 on Wayland, but with Chrome running on XWayland. However, it does not work on pure Wayland, and nor does it work on my desktop with an Nvidia GPU on Manjaro (X11), nor on an older laptop with an Intel iGPU on Arch (Wayland).

```sh 
google-chrome --use-gl=angle \
    --use-angle=vulkan \
    --enable-accelerated-video-decode \
    --enable-features=Vulkan,VulkanFromANGLE,DefaultANGLEVulkan,VaapiIgnoreDriverChecks,VaapiVideoDecoder,PlatformHEVCDecoderSupport,UseMultiPlaneFormatForHardwareVideo
```

Chrome provides a GPU information page at chrome://gpu, but I don't think it is meaningful, as it possible to see "Video Decode: Hardware accelerated" while still using a software decoder to actually play a video.

The way to check that hardware decoding is really working is to play a [video](https://www.youtube.com/watch?v=dQw4w9WgXcQ) and see what decoder is in use by visiting chrome://media-internals/ and opening the information for the active player. 

Scroll down to the kVideoDecoderName and see what decoder it is using.

<a href='/media/chrome-hardware-acceleration/chrome.png' target=_blank>![chrome://media-internals output](/media/chrome-hardware-acceleration/chrome.png)</a>


If you see VaapiVideoDecoder, then you are using hardware decoding.<br/>
If you see Dav1dVideoDecoder, then you are using software decoding.

There are other possible values but I don't have an exhaustive list.