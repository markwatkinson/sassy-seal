---
title: "Lenovo Legion Slim 5 AMD on Linux"
lastUpdated: "2024-12-27"
template: "post"
draft: false
slug: "linux/lenovo-legion-slim-5-amd"
description: "Experiences with getting Linux working smoothly on a modern laptop"
---

These are my experiences with getting Linux working on a Lenovo Legion Slim 5 laptop on Linux.

My configuration of the laptop is 16APH8 with an AMD 7840HS + Radeon 780M integrated GPU (IGPU) and an NVIDIA 4070 discrete GPU (DGPU). Some of my experiences may be different to the Intel CPU based configurations, especially regarding DGPU power management!

I first installed Arch but I had issues with the DGPU's power management. Someone on NVIDIA's forums claimed to have it working with Fedora, so I tried Fedora 40 instead. I didn't find the suggestions worked for me, so I'm basically on Fedora by accident, but I'm sticking with it for now as my old laptop is on Arch anyway, and I've no complaints against Fedora (except that it often wants to reboot after updating). In fact, Fedora was the first Linux distribution I ever used, back in 2006.

Apart from where otherwise stated, everything on this page was tested under Fedora 40 (KDE spin) on kernel 6.8.7-300.fc40.x86_64.

Here are the annoyances I've found:
<div class='contents'>

- [Black screen boot](#black-screen-boot)
- [Unresponsive keyboard/dropped keystrokes](#unresponsive-keyboarddropped-keystrokes)
- [Sleep](#sleep)
- [NVIDIA DGPU power consumption](#nvidia-dgpu-power-consumption)
  - [A working config](#a-working-config)
  - [Power draw, battery life and comparisons to Windows](#power-draw-battery-life-and-comparisons-to-windows)
  - [Power management under the proprietary drivers](#power-management-under-the-proprietary-drivers)
  - [Other things I tried](#other-things-i-tried)
    - [acpi\_call](#acpi_call)
    - [PCI device /remove message](#pci-device-remove-message)
  - [Useful resources](#useful-resources)
  - [Conclusions](#conclusions)
- [External monitors](#external-monitors)

</div>


## Black screen boot

On Arch (live USB installer), the machine would boot and then seemingly freeze on a black screen, needing a power off to reset it. I believe this is because it couldn't handle the screen's high refresh rate before we get to a graphical environment. You can work around this by adding `video=2560x1600@60` to your GRUB boot parameters to lock it to 60hz. This does not seem to prevent later setting it to 165Hz once you get into a graphical environment (though I can't see the difference, so who knows).

You can edit the line by pressing 'e' on the GRUB boot menu, editing it, then control-x to launch it.

The whole line will look something like:

`linux /boot/vmlinuz root=UUID=6ffddfad-bc66-4935-8f85-b514144e7340 rw loglevel 3 quiet video=2560x1600@60`

You will have to set this after installation too. 

Fedora does not have this issue. I *think* this is because Arch uses the modesetting driver whereas Fedora comes configured to use the AMDGPU driver, but I haven't yet reinstalled Arch to find out. Therefore you probably will not need to make it permanent if you use set up the AMDGPU driver.

## Unresponsive keyboard/dropped keystrokes

The keyboard will occasionally seem to drop keystrokes. This is because the USB bus the keyboard is connected to goes into a low power mode. (This is also a problem on Windows too)

To prevent this, I am using the following script to put the USB device into always on mode.

```
## usbpower.sh
## Run as root/sudo
echo 'on' > '/sys/bus/usb/devices/usb1/power/control'
```

It's set to usb1 because USB bus 1 is where a lot of the internals are connected. You can work this out from `lsusb -t`

<a href='/media/20240427/lsusb-t.png' target=_blank>![lsusb -t output. I have 3 HCI devices under bus 001 port 005](/media/20240427/lsusb-t.png)</a>


## Sleep

Sleep seems to work out of the box, except that sometimes the system completely dies while asleep and when you try to wake it up you end up powering it on as if it had been shut down. Nothing is written into any logs in this case, so it's a bit of a mystery what actually happens.

This seems to be resolved by blacklisting the ideapad_laptop module, with:

`echo "blacklist ideapad_laptop" >> /etc/modprobe.d/blacklist.conf`

This actually means sleep works better than it does on Windows, where sleep results in the fans spinning up every 10 minutes.



## NVIDIA DGPU power consumption


**Important** 2024-12-27:  This section is out of date, but left for posterity. I have now got this working, please see [Powering down your idle Nvidia dGPU in Linux](../nvidia-dgpu-power)

------

This section was tested on the NVIDIA drivers version 550. I haven't yet tried on 555 but there's nothing in the [changelog](https://www.nvidia.com/Download/driverResults.aspx/224751/en-us/) to indicate that they will be any better.

If anyone has anything to add to this section or thinks something I've written here is wrong, please email mark [at] markwatkinson [dot] com. It's a complex topic with a lot of variables and I may have missed something or made mistakes.

### A working config

My current set up for acceptable power draw is to use Nouveau drivers and use [envycontrol](https://github.com/bayasdev/envycontrol) in integrated mode. This means that the DGPU is not really usable at all under Linux, which is not ideal, but, for the moment for me personally, it is preferable to having a short battery life. 

In the longer term I hope to get the DGPU powering down properly when idle under the proprietary drivers, but maybe I'll need to wait for driver and/or kernel updates before then.

But even this isn't perfect - the PCIE bus can wake up and start powering the DGPU if you or a program runs `lspci`. If this happens, the only solution I've found is to sleep and wake or restart the laptop.

One such offender is Steam, which runs `lspci` on start up. I have uninstalled pciutils (which provides lspci on Fedora) because of this (which fortunately isn't marked as a dependency for anything I care about).

Note you don't really need to use envycontrol. All it does it create a blacklist and a udev file. See [lines 17-56](https://github.com/bayasdev/envycontrol/blob/e10ac7e566b77a15dd7129be3c55a0e49878b8ff/envycontrol.py#L17)

I am not sure why I need the Nouveau drivers for this, as in theory the NVIDIA side is irrelevant and blacklisted when envycontrol is on integrated mode.

Some more research follows.

### Power draw, battery life and comparisons to Windows

Under Windows, where things work out of the box, the device draws roughly 10W with the DGPU inactive and 25W while active. Active means that some software is running to keep it awake (e.g. monitoring software like GPU-Z), but not actually putting any load through it. In the inactive state, the DGPU is in power state D3. The power numbers are obviously also affected by other factors like what other programs are running, bluetooth/wifi state, the screen brightness etc., I'm just using 10 and 25 as approximate round numbers that give an indication into whether the DGPU is on or off (in reality it's drawing 11.7W as I write this, but can drop below 10W easily).

10W gives a battery life of around 7 hours, 25W gives less than 3. So, to me, having an idle GPU more than halving my battery life is totally unreasonable. For others who use the DGPU often and keep it plugged in, your feelings will be different.

### Power management under the proprietary drivers

On Linux, the proprietary NVIDIA drivers [claim they will put the device into D3 when appropriate](https://download.nvidia.com/XFree86/Linux-x86_64/550.78/README/dynamicpowermanagement.html). In reality this just doesn't happen, so the system draws around 25W of power as a baseline.

A [thread on the NVIDIA forums](https://forums.developer.nvidia.com/t/nvidia-gpu-fails-to-power-off-prime-razer-blade-14-2022/250023) has people saying it hasn't worked for any driver version later than 525. There are some solutions and workarounds but none of them worked for me. Jaynope's Fedora suggestions got me to the point that the DGPU will go into d3cold, but it won't stay there for long, so it doesn't really help. nvidia-smi always shows one Xorg process running on the card, taking up about 4MB of VRAM (even if I'm in Wayland). 

**note**: I wonder now if this came from [SDDM not being in Wayland mode](https://wiki.archlinux.org/title/SDDM#Wayland), but there is no Xorg process running currently using integrated/nouveau, so it may just be an artefact of how the proprietary drivers work.

### Other things I tried

I've tried various things to try to keep the DGPU available but powered down.

#### acpi_call

acpi_call is a kernel module that lets you write ACPI messages from userspace. Some distros include it in their repositories, or you can get and compile it yourself from the [Nix community github repo](https://github.com/nix-community/acpi_call)

```
# load the module
modprobe acpi_call

# power off
echo "\_SB.PCI0.GPP0.PG00._OFF" > /proc/acpi/call

#power on
echo "\_SB.PCI0.GPP0.PG00._ON" > /proc/acpi/call
```

This sends the _OFF message to the GPU. Under X11 this successfully powers it off, but not under Wayland. Sending the corresponding _ON call powers it back up, however, it's not usable until you reboot. The laptop is unstable after the _OFF call and crashes on sleep and logout, and sometimes on opening applications.

I thought I found in the NVIDIA driver documentation that these calls were officially supported, but I can't find it now so I'm not sure.

#### PCI device /remove message

You can remove devices from the PCI bus with a command like the following:

```
echo 1 > /sys/bus/pci/devices/0000\:01\:00.0/remove
echo 1 > /sys/bus/pci/devices/0000\:01\:00.1/remove
````

The first device is the DGPU's video interface, the second is the DGPU's HDMI audio interface.

Under the NVIDIA driver, the command to 0000:01:00.0 just hangs (but to .1 it does succeed). 

Under Nouveau + X11 it works and powers down the GPU, but the machine crashed on shutdown so I suspect it would be unstable in everyday use. Under Nouveau and Wayland nothing happens and power usage remains at ~25W.

### Useful resources
Other resources and discussions I've found:

  - [Discussion on DGPU not powering down on >525 drivers. Semi active](https://forums.developer.nvidia.com/t/nvidia-gpu-fails-to-power-off-prime-razer-blade-14-2022/250023)
  - [Discussion on Xorg showing up under nvidia-smi](https://forums.developer.nvidia.com/t/xorg-still-in-gpu-with-prime-offload-and-dynamic-power-management/170485)
  - [NVIDIA docs on D3 power management](https://http.download.nvidia.com/XFree86/Linux-x86_64/550.54.14/README/dynamicpowermanagement.html)
  - [Arch Wiki's PRIME article](https://wiki.archlinux.org/title/PRIME#PRIME_render_offload)
  - [Gentoo Wiki's guide to getting to d3 cold](https://wiki.gentoo.org/wiki/Razer_Blade_Pro_(2019)#NVIDIA)
  - [Lenovo Legion Linux project describing a configuration](https://github.com/johnfanv2/LenovoLegionLinux/tree/cba5b7043f58c2f577b46c56a61e0143708c4e8e?tab=readme-ov-file#how-to-power-down-the-dgpu-while-not-in-use-and-use-the-igpu-for-power-saving-instead). This doesn't contain anything I didn't try but is a concise guide to what should work. Note that 1. It says nvidia-smi showing Xorg is OK, and 2. It says this only works on X11.

### Conclusions

The DGPU issue situation is frustrating. It's a complex situation with many variables. It shows a little more promise under X11 than Wayland (perhaps unsurprisingly), but in the end it doesn't appear possible currently to use the DGPU and have sensible power management. It's very possible that this works better on Intel CPUs though.

I intend to revisit this as newer kernels and drivers are released. In the meantime, if anyone has anything to add or thinks something I've written here is wrong, please email mark [at] markwatkinson [dot] com.

## External monitors

The HDMI port at the back of the device does not work at all. I think the HDMI port is wired to the DGPU, which I've disabled, so that's not surprising.

However, external monitors seem to work OK via a USB-C dock. The first time I tried to plug one in Plasma/Wayland showed the second monitor as a pure white screen and would not let me do anything with it except move the cursor across it. I tried logging out to switch to X11 and SDDM kept flickering the screen image from the external monitor to the built in and vice versa, then the device crashed entirely when I tried to log in and I had to power it off.

But upon reboot everything seemed to work fine with Plasma, Wayland and HDMI through USB-C. Wayland was happy with the built in set to 165hz and the external at 60hz (though I don't think Xorg can handle different refresh rates). 

I haven't tested this extensively as I rarely use an external monitor, but it has worked as expected on a few occasions since the initial problem.

We'll see how this goes. It could just be a Plasma 6 issue which will get worked out in time.