---
title: "Full Disk Encryption"
lastUpdated: "2024-05-01"
draft: false
slug: "linux/full-disk-encryption"
description: ""
---

A lot of Linux installers now support full disk encryption (FDE). Disk encryption is complex and warrants some thought. 

True FDE comes with a downside that decryption is done pre-boot when hardware acceleration is not available, so the initial decryption is extremely slow, with the message "Attempting to decrypt master key". This can easily add 10 to 20 seconds to boot time. This is the case in current (May 2024) versions of the openSUSE Tumbleweed installer, and probably other distributions' installers too.

There are ways around this with alternative implementations of disk encryption, but exactly which works best depends on individual requirements.

<div class=contents>

- [Partial disk encryption](#partial-disk-encryption)
- [The evil maid](#the-evil-maid)
- [Security concerns](#security-concerns)

</div>


## Partial disk encryption

An alternative to full disk encryption is partial disk encryption. There are various layouts for a partially encrypted system, but, in order to work around a slow boot, all you need to do is leave /boot unencrypted. So you end up with a /boot partition and everything else under an encrypted LVM container, like this:

<pre class='raw'>
||============================================================================||
|| **************  ********************************************************** ||
|| *            *  *            LVM container, encrypted with LUKS          * ||
|| *            *  ********************************************************** ||
|| *   /boot    *  *                  *                  *                  * ||
|| *   FAT32    *  *        /         *      /home       *       swap       * ||
|| *            *  *       ext4       *       ext4       *      swapfs      * ||
|| *            *  *                  *                  *                  * ||
|| **************  ********************************************************** ||
||============================================================================||
</pre>


Another layout would be to have a separate home partition and just encrypt that. The downside of this is that data can leak out of /home, for example into /tmp or the swap partition. To protect against this, /tmp can be mounted as tmpfs, i.e. in RAM. Swap can either be moved to a swap file stored in /home (i.e. encrypted), or it can be kept as a partition and encrypted separately with a random key chosen at boot. The latter provides stronger security, and looks something like this:

<pre class='raw'>
||============================================================================||
|| **************  ********************************************************** ||
|| *            *  *                  *                  *                  * ||
|| *   /boot    *  *        /         *      /home       *       swap       * ||
|| *   FAT32    *  *       ext4       *    ext4 (LUKS)   *   swapfs (LUKS)  * ||
|| *            *  *                  *                  *                  * ||
|| **************  ********************************************************** ||
||============================================================================||
</pre>

Both of these are largely equivalent from a usability point of view in that the boot will be fast and the password will be requested once. The first option is more secure as (almost) the whole system is encrypted, but it needs to be set up at installation time. The second layout can be applied relatively easily to a non-encrypted running system (although the /home partition will need to be reformatted).

The downside of both of these is that they are susceptible to something called an evil maid attack, the second option more so than the first.

## The evil maid

Briefly, an [evil maid attack](https://en.wikipedia.org/wiki/Evil_maid_attack) is a security attack by somebody (e.g. your surprisingly technical maid) who has physical access to your device and is able to tamper with it. In the case of the unencrypted /boot partition, the attacker could substitute a kernel image for their own compromised version, which would be a catastrophic security failure for the victim.
 
FDE mitigates this by making sure that nothing is accessible to the maid. The maid cannot tamper with the kernel images because the kernel images are encrypted. Secure boot also goes some way to preventing an attack on the kernel image, though can be complicated to implement.

Note, however, that FDE is a software solution and there are still hardware attacks available to the maid - for example, using a hardware keylogger between your keyboard and USB port to intercept your decryption password.

## Security concerns

It's important to consider exactly what you are trying to achieve with disk encryption, and whether the evil maid attack is one which you think is a realistic attack against yourself.

Disk encryption is effective at stopping data leakage through device loss, e.g. if someone steals your laptop and then decides to look at what's on the disk, or if you sell the laptop after deleting private files but someone runs disk recovery software on it. 

It's not particularly effective at dealing with someone motivated who is willing to threaten violence, e.g. a criminal or state sponsored organisations like intelligence, military or police services, at least, assuming you are receptive to threats of violence.

The evil maid attack is a sophisticated attack, which requires significant technical expertise. It also requires physical access.

Whether or not this is something that needs to be worried about depends highly on individual circumstances. Most people don't have adversaries who would be motivated and able to perform such an attack. But the existence of adversaries can be surprising. The [xz backdoor attempt](https://en.wikipedia.org/wiki/XZ_Utils_backdoor) shows that innocuous software projects can be targeted by malicious actors looking to insert general backdoors, so it follows that low profile developers may also be targeted in ways they may find surprising.